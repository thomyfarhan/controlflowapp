package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        etClassificationNilai.setText("0")
        btnClassificationShow.setOnClickListener {
            val nilai = if (etClassificationNilai.text.isEmpty()) -1 else etClassificationNilai.text.toString().toInt()
            doClassification(nilai)
        }
    }

    fun doClassification(nilai: Int) {
        when {
            nilai == -1 -> Toast.makeText(this,"Nilai Tidak Boleh Kosong!!", Toast.LENGTH_LONG).show()
            nilai > 1000 -> Toast.makeText(this, "Nilai maksimal tidak lebih dari 1000!!!!", Toast.LENGTH_LONG).show()
            else -> tvClassificationHasil.text = when(nilai) {
                        in 0..70 -> "Hasilnya: Tidak Lulus!"
                        in 71..80 -> "Hasilnya: Lulus Aja!"
                        in 81..100 -> "Hasilnya: Lulus Banget!!"
                        else -> "Hasilnya: Nilai Error!!!"
                    }
        }
    }
}
