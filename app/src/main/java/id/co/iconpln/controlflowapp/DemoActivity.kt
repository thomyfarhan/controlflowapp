package id.co.iconpln.controlflowapp

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.SearchView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_demo.*

class DemoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        Log.d("lifecycle", "start onCreate")

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnDemoSubmit.setOnClickListener(this)
        btnDemoSnackbar.setOnClickListener(this)
        btnDemoSnackbarButton.setOnClickListener(this)
        btnDemoSnackbarCustom.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnDemoSubmit -> startActivity(Intent(this, StyleActivity::class.java))
            R.id.btnDemoSnackbar -> {
                Snackbar.make(clDemo, "This is Snackbar", Snackbar.LENGTH_SHORT).show()
            }
            R.id.btnDemoSnackbarButton -> {
                Snackbar
                    .make(clDemo, "Message is deleted", Snackbar.LENGTH_SHORT)
                    .setAction("Undo", undoListener)
                    .show()
            }
            R.id.btnDemoSnackbarCustom -> {
                val customSnackbar = Snackbar
                    .make(clDemo, "This is Custom Snackbar", Snackbar.LENGTH_SHORT)
                    .setAction("Undo", undoListener)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight))

                val snackbarView = customSnackbar.view

                val textSnackbar: TextView = snackbarView.findViewById(R.id.snackbar_text)
                textSnackbar.apply {
                    setTextColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
                }

                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))

                customSnackbar.show()
            }
        }
    }

    private val undoListener = object: View.OnClickListener {
        override fun onClick(p0: View?) {
            Snackbar.make(clDemo, "Message is restore", Snackbar.LENGTH_SHORT).show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_demo, menu)
        setupSearchView(menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun setupSearchView(menu: Menu?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.action_demo_search)?.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = getString(R.string.search_hint)

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Toast.makeText(this@DemoActivity, query, Toast.LENGTH_SHORT).show()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Toast.makeText(this@DemoActivity, newText, Toast.LENGTH_SHORT).show()
                return true
            }

        })
    }

    override fun onStart() {
        super.onStart()

        Log.d("lifecycle", "start onStart")
    }

    override fun onResume() {
        super.onResume()

        Log.d("lifecycle", "start onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.d("lifecycle", "start onPause")
    }

    override fun onStop() {
        super.onStop()

        Log.d("lifecycle", "start onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d("lifecycle", "start onDestroy")
    }

    override fun onRestart() {
        super.onRestart()

        Log.d("lifecycle", "start onRestart")
    }
}
