package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.backgroundThread.BackgroundThreadActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreferences.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnPangkat -> startActivity(Intent(this, MainActivity::class.java))
            R.id.btnKlasifikasi -> startActivity(Intent(this, ClassificationActivity::class.java))
            R.id.btnHomeLogin -> startActivity(Intent(this,LoginActivity::class.java))
            R.id.btnHomeOperation -> startActivity(Intent(this, OperationActivity::class.java))
            R.id.btnHomeStyle -> startActivity(Intent(this, StyleActivity::class.java))
            R.id.btnHomeActivity -> startActivity(Intent(this, DemoActivity::class.java))
            R.id.btnHomeVolume -> startActivity(Intent(this, VolumeActivity::class.java))
            R.id.btnHomeIntent -> startActivity(Intent(this, IntentActivity::class.java))
            R.id.btnHomeComplex -> startActivity(Intent(this, ComplexConstraintActivity::class.java))
            R.id.btnHomeConstraint -> startActivity(Intent(this, ConstraintActivity::class.java))
            R.id.btnHomeListHero -> startActivity(Intent(this, ListHeroActivity::class.java))
            R.id.btnHomeGridHero -> startActivity(Intent(this, GridHeroActivity::class.java))
            R.id.btnHomeDemoFragment -> startActivity(Intent(this, DemoFragmentActivity::class.java))
            R.id.btnHomeTab -> startActivity(Intent(this, TabActivity::class.java))
            R.id.btnHomeBottomNav -> startActivity(Intent(this, BottomNavActivity::class.java))
            R.id.btnHomeNavDrawer -> startActivity(Intent(this, NavDrawerActivity::class.java))
            R.id.btnHomeBottomSheet -> startActivity(Intent(this, BottomSheetActivity::class.java))
            R.id.btnHomeLocalization -> startActivity(Intent(this, LocalizationActivity::class.java))
            R.id.btnHomeScroll -> startActivity(Intent(this, ScrollActivity::class.java))
            R.id.btnHomePref -> startActivity(Intent(this, SharedPreferencesActivity::class.java))
            R.id.btnHomeWeather -> startActivity(Intent(this, WeatherActivity::class.java))
            R.id.btnHomeContact -> startActivity(Intent(this, ContactActivity::class.java))
            R.id.btnHomeBackgroundThread -> startActivity(Intent(this, BackgroundThreadActivity::class.java))
            R.id.btnHomeContactTab -> startActivity(Intent(this, ContactTabActivity::class.java))
            R.id.btnHomeMyContact -> startActivity(Intent(this, MyContactActivity::class.java))
            R.id.btnHomeMyUser -> startActivity(Intent(this, MyUserActivity::class.java))
            R.id.btnHomeProfile -> startActivity(Intent(this, MyProfileActivity::class.java))
        }
    }

    private fun setOnClickButton() {
        btnPangkat.setOnClickListener(this)
        btnKlasifikasi.setOnClickListener(this)
        btnHomeLogin.setOnClickListener(this)
        btnHomeOperation.setOnClickListener(this)
        btnHomeStyle.setOnClickListener(this)
        btnHomeActivity.setOnClickListener(this)
        btnHomeVolume.setOnClickListener(this)
        btnHomeIntent.setOnClickListener(this)
        btnHomeComplex.setOnClickListener(this)
        btnHomeConstraint.setOnClickListener(this)
        btnHomeListHero.setOnClickListener(this)
        btnHomeGridHero.setOnClickListener(this)
        btnHomeDemoFragment.setOnClickListener(this)
        btnHomeTab.setOnClickListener(this)
        btnHomeBottomNav.setOnClickListener(this)
        btnHomeNavDrawer.setOnClickListener(this)
        btnHomeBottomSheet.setOnClickListener(this)
        btnHomeLocalization.setOnClickListener(this)
        btnHomeScroll.setOnClickListener(this)
        btnHomePref.setOnClickListener(this)
        btnHomeWeather.setOnClickListener(this)
        btnHomeContact.setOnClickListener(this)
        btnHomeBackgroundThread.setOnClickListener(this)
        btnHomeContactTab.setOnClickListener(this)
        btnHomeMyContact.setOnClickListener(this)
        btnHomeMyUser.setOnClickListener(this)
        btnHomeProfile.setOnClickListener(this)
    }
}
