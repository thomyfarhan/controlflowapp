package id.co.iconpln.controlflowapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

private const val REQUEST_CODE = 110

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val btnMoveActivity: Button
        get() = btn_move_activity

    private val btnMoveActivityData: Button
        get() = btn_move_activity_data

    private val btnMoveActivityBundle: Button
        get() = btn_move_activity_bundle

    private val btnMoveActivityObject: Button
        get() = btn_move_activity_object

    private val btnIntentImplicit: Button
        get() = btn_intent_implicit

    private val btnIntentOpenWeb: Button
        get() = btn_intent_open_web

    private val btnIntentSendSms: Button
        get() = btn_intent_send_sms

    private val btnIntentShowMap: Button
        get() = btn_intent_show_map

    private val btnIntentShareText: Button
        get() = btn_intent_share_text

    private val btnIntentResult: Button
        get() = btn_intent_result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        setButtonClickHandler()
    }

    private fun setButtonClickHandler() {
        btnMoveActivity.setOnClickListener(this)
        btnMoveActivityData.setOnClickListener(this)
        btnMoveActivityBundle.setOnClickListener(this)
        btnMoveActivityObject.setOnClickListener(this)
        btnIntentImplicit.setOnClickListener(this)
        btnIntentOpenWeb.setOnClickListener(this)
        btnIntentSendSms.setOnClickListener(this)
        btnIntentShowMap.setOnClickListener(this)
        btnIntentShareText.setOnClickListener(this)
        btnIntentResult.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btn_move_activity -> {
                val intentMove = Intent(this, StyleActivity::class.java)
                startActivity(intentMove)
            }
            R.id.btn_move_activity_data -> {
                val intentMoveData = Intent(this, IntentMoveDataActivity::class.java)
                intentMoveData.putExtra(IntentMoveDataActivity.EXTRA_NAME, "Thompson")
                intentMoveData.putExtra(IntentMoveDataActivity.EXTRA_AGE, 52)
                startActivity(intentMoveData)
            }
            R.id.btn_move_activity_bundle -> {
                val intentMoveBundle = Intent(this, IntentMoveBundleActivity::class.java)
                val bundle = Bundle()

                bundle.putString(IntentMoveBundleActivity.EXTRA_NAME, "Thompson")
                bundle.putInt(IntentMoveBundleActivity.EXTRA_AGE, 14)
                intentMoveBundle.putExtras(bundle)
                startActivity(intentMoveBundle)
            }
            R.id.btn_move_activity_object -> {
                val person = Person("Thompson", 33)
                val intentMoveObject = Intent(this, IntentMoveObjectActivity::class.java)

                intentMoveObject.putExtra(IntentMoveObjectActivity.EXTRA_PERSON, person)
                startActivity(intentMoveObject)
            }
            R.id.btn_intent_implicit -> {
                val phone = "15000"
                val intentImplicit = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phone"))

                startActivity(intentImplicit)
            }
            R.id.btn_intent_result -> {
                val intentResult = Intent(this, IntentWithResultActivity::class.java)
                startActivityForResult(intentResult, REQUEST_CODE)
            }
            R.id.btn_intent_open_web -> {
                val webpage = Uri.parse("https://www.binar.co.id")
                val intentOpenWeb = Intent(Intent.ACTION_VIEW, webpage)

                if (intentOpenWeb.resolveActivity(packageManager) != null) {
                    startActivity(intentOpenWeb)
                }
            }
            R.id.btn_intent_send_sms -> {
                val phone = "15000"
                val sendSms = Uri.parse("smsto: $phone")
                val message = "Halo Dunia"
                val intentSendSms = Intent(Intent.ACTION_SENDTO, sendSms)
                intentSendSms.putExtra("sms_body", message)

                if (intentSendSms.resolveActivity(packageManager) != null) {
                    startActivity(intentSendSms)
                }
            }
            R.id.btn_intent_show_map -> {
                val latitude = "47.6"
                val longitude = "-122.3"
                val showMap = Uri.parse("geo: $latitude, $longitude")
                val intentShowMap = Intent(Intent.ACTION_VIEW, showMap)

                if (intentShowMap.resolveActivity(packageManager) != null) {
                    startActivity(intentShowMap)
                }
            }
            R.id.btn_intent_share_text -> {
                val sharedText = "Ini teks"

                val intentShareText = Intent(Intent.ACTION_SEND)
                intentShareText.putExtra(Intent.EXTRA_TEXT, sharedText)
                intentShareText.type = "text/plain"

                val shareIntent = Intent.createChooser(intentShareText, "Pilih yang mana")

                if(shareIntent.resolveActivity(packageManager) != null) {
                    startActivity(shareIntent)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == IntentWithResultActivity.RESULT_CODE) {
                val value = data?.getIntExtra(
                    IntentWithResultActivity.EXTRA_VALUE, 0)
                btn_intent_result.text = getString(R.string.intent_with_result_text, value)
            }
        }
    }
}
