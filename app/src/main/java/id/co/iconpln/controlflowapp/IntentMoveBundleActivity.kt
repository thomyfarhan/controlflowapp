package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_intent_move_bundle.*

class IntentMoveBundleActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NAME = "name"
        const val EXTRA_AGE = "age"
    }

    private val tvIntentBundleReceived: TextView
        get() = tv_intent_bundle_received

    private var name: String = ""
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_bundle)

        getIntentExtras()
        displayData()
    }

    private fun getIntentExtras() {
        val bundle = intent.extras
        name = bundle?.getString(EXTRA_NAME) ?: ""
        age = bundle?.getInt(EXTRA_AGE) ?: 0
    }

    private fun displayData() {
        val text = "Name: $name, Age: $age"
        tvIntentBundleReceived.text = text
    }
}
