package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_intent_move_data.*

class IntentMoveDataActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NAME = "name"
        const val EXTRA_AGE = "age"
    }

    private val tvIntentDataReceived: TextView
        get() = tv_intent_data_received

    private var name: String = ""
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_data)

        getIntentExtra()
        showData()
    }

    private fun getIntentExtra() {
        name = intent.getStringExtra(EXTRA_NAME) ?: ""
        age = intent.getIntExtra(EXTRA_AGE, 0)
    }

    private fun showData() {
        val text = "Nama: $name, Umur: $age"
        tvIntentDataReceived.text = text
    }
}
