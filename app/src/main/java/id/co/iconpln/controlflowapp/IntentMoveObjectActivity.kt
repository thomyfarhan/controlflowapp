package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_move_object.*

class IntentMoveObjectActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PERSON = "person"
    }

    private val tvIntentObjectReceived: TextView
        get() = tv_intent_object_received

    private lateinit var person: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_object)

        getIntentExtras()
        displayData()
    }

    private fun getIntentExtras() {
        person = intent.getParcelableExtra(EXTRA_PERSON) ?: Person("", 0)
    }

    private fun displayData() {
        val text = "Name: ${person.name}, Age: ${person.age}"
        tvIntentObjectReceived.text = text
    }
}
