package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioGroup
import kotlinx.android.synthetic.main.activity_intent_with_result.*

class IntentWithResultActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_VALUE = "value"
        const val RESULT_CODE = 110
    }

    private val btnIntentResultChoose: Button
        get() = btn_intent_result_choose

    private val rgIntentResultNumber: RadioGroup
        get() = rg_intent_result_number

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_result)

        setOnClickHandler()
    }

    private fun setOnClickHandler() {
        btnIntentResultChoose.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btn_intent_result_choose -> {
                if (rgIntentResultNumber.checkedRadioButtonId != 0) {
                    val value = when(rgIntentResultNumber.checkedRadioButtonId) {
                        R.id.rb_intent_result_50 -> 50
                        R.id.rb_intent_result_100 -> 100
                        R.id.rb_intent_result_150 -> 150
                        R.id.rb_intent_result_200 -> 200
                        else -> 0
                    }

                    val resultIntent = Intent()
                    resultIntent.putExtra(EXTRA_VALUE, value)
                    setResult(RESULT_CODE, resultIntent)
                    finish()
                }
            }
        }
    }
}
