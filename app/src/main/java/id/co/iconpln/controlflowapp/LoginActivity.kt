package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setOnClickButton()
        // TODO:
        //  1. Userame & password tidak boleh empty
        //  2. Password min. 7 digit
        //  3. akan sukses jika login: user@mail.com, password
        //  4. username mengecek format email
    }

    private fun setOnClickButton() {
        btnLoginSubmit.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id) {
            R.id.btnLoginSubmit -> checkField()
        }
    }

    private fun checkField() {
        tvLoginStatus.text = "Status:"
        if (etLoginEmail.text.isEmpty()) {
            etLoginEmail.error = "Email jangan kosong!"
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.text).matches()) {
            etLoginEmail.error = "Email yang bener!"
        } else if (etLoginPassword.text.isEmpty()) {
            etLoginPassword.error = "Password jangan kosong!"
        } else if (etLoginPassword.text.length < 7) {
            etLoginPassword.error = "Password min. 7 digit!"
        } else {
            tvLoginStatus.text = "Status: Login Sukses!"
        }
    }
}
