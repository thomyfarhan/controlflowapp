package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etNilai.setText("0")
        btnShow.setOnClickListener {
            val nilai = if (etNilai.text.isEmpty()) 0.0 else etNilai.text.toString().toDouble()
            showHasil(hitungPangkatDua(nilai))
        }
    }

    fun hitungPangkatDua(nilai: Double) = nilai.pow(2.0)

    fun showHasil(hasil: Double) {
        Toast.makeText(this, "$hasil", Toast.LENGTH_LONG).show()
        tvHasil.text = "Hasilnya $hasil"
    }
}
