package id.co.iconpln.controlflowapp

sealed class Operation {
    class Sum (val value: Double): Operation()
    class Divide (val value: Double): Operation()
    class Multiply (val value: Double): Operation()
    class Substract (val value: Double): Operation()
}
