package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*
import java.text.DecimalFormat

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Double = 0.0
    private var inputY: Double = 0.0

    private lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()
    }

    private fun displayResult() {
        tvOperationResult.text = DecimalFormat("0.#").format(operationViewModel.operationResult)
        tvOperationOperator.text = operationViewModel.operator
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun getInputNumbers() {
        if(etOperationBilanganX.text?.isNotEmpty() == true &&
            etOperationBilanganY.text?.isNotEmpty() == true) {
            inputX = etOperationBilanganX.text.toString().toDouble()
            inputY = etOperationBilanganY.text.toString().toDouble()
        }
    }

    private fun setButtonClickListener() {
        btnOperationSum.setOnClickListener(this)
        btnOperationDivide.setOnClickListener(this)
        btnOperationMultiply.setOnClickListener(this)
        btnOperationSubstract.setOnClickListener(this)
        btnOperationReset.setOnClickListener(this)
        btnOperationCalculate.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnOperationSum -> operationViewModel.operator = "+"
            R.id.btnOperationDivide -> operationViewModel.operator = "/"
            R.id.btnOperationMultiply -> operationViewModel.operator = "x"
            R.id.btnOperationSubstract -> operationViewModel.operator = "-"
            R.id.btnOperationCalculate -> calculateOperation()
            R.id.btnOperationReset -> resetOperation()
        }
        tvOperationOperator.text = operationViewModel.operator
    }

    private fun resetOperation() {
        etOperationBilanganX.setText(R.string.zero_number_text)
        etOperationBilanganY.setText(R.string.zero_number_text)
        tvOperationResult.text = getString(R.string.zero_number_text)
        inputX = 0.0
        inputY = 0.0
        operationViewModel.operationResult = 0.0
        operationViewModel.operator = ""
    }

    private fun calculateOperation() {
        getInputNumbers()
        operationViewModel.operation = getCalculateOperation()
        if (operationViewModel.operation == null) {
            Toast.makeText(this, "Pilih Operator", Toast.LENGTH_LONG).show()
        } else if(operationViewModel.operation is Operation.Divide && inputY == 0.0) {
            Toast.makeText(this,"INFINITY!", Toast.LENGTH_LONG).show()
            tvOperationResult.text = getString(R.string.infinity_text)
        } else {
            operationViewModel.operationResult = operationViewModel.execute(inputY, operationViewModel.operation!!)
            displayResult()
        }
    }

    private fun getCalculateOperation(): Operation? {
        return when(operationViewModel.operator) {
            getString(R.string.btn_operation_sum_text) -> Operation.Sum(inputX)
            getString(R.string.btn_operation_divide_text) -> Operation.Divide(inputX)
            getString(R.string.btn_operation_multiply_text) -> Operation.Multiply(inputX)
            getString(R.string.btn_operation_substract_text) -> Operation.Substract(inputX)
            else -> null
        }
    }

}
