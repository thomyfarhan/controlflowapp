package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class OperationViewModel: ViewModel() {

    var operationResult = 0.0
    var operator = ""
    var operation: Operation? = null

    fun execute(number: Double, operation: Operation): Double {
        return when (operation) {
            is Operation.Sum -> operation.value + number
            is Operation.Divide -> operation.value / number
            is Operation.Multiply -> operation.value * number
            is Operation.Substract -> operation.value - number
        }
    }

}