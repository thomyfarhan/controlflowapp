package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*
import java.text.DecimalFormat

class VolumeActivity : AppCompatActivity(), View.OnClickListener {

    private val btnVolumeCalculate: Button
        get() = btn_volume_calculate
    private val etVolumeLength: EditText
        get() = et_volume_length
    private val etVolumeWidth: EditText
        get() = et_volume_width
    private val etVolumeHeight: EditText
        get() = et_volume_height
    private val tvVolumeResult: TextView
        get() = tv_volume_result

    private lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        initViewModel()
        displayResult()
        setClickHandlerButton()
    }

    private fun displayResult() {
        tvVolumeResult.text = getString(R.string.tv_volume_result_text,
            DecimalFormat("0.#").format(volumeViewModel.volumeResult))
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this)
            .get(VolumeViewModel::class.java)
    }

    private fun setClickHandlerButton() {
        btnVolumeCalculate.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btn_volume_calculate -> calculateVolume()
        }
    }

    private fun calculateVolume() {
        if (checkField()) {
            val length = etVolumeLength.text.toString().toDouble()
            val width = etVolumeWidth.text.toString().toDouble()
            val height = etVolumeHeight.text.toString().toDouble()
            volumeViewModel.volumeResult = volumeViewModel.calculate(length, width, height)

            displayResult()
        }
    }

    private fun checkField(): Boolean {
        when {
            etVolumeLength.text.isEmpty() -> etVolumeLength.error = "Length tidak boleh kosong"
            etVolumeWidth.text.isEmpty() -> etVolumeWidth.error = "Width tidak boleh kosong"
            etVolumeHeight.text.isEmpty() -> etVolumeHeight.error = "Height tidak boleh kosong"
            else -> return true
        }
        return false
    }


}
