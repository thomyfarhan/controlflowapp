package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class VolumeViewModel : ViewModel() {
    var volumeResult: Double = 0.0

    fun calculate(length: Double, width: Double, height: Double): Double {
        return length * width * height
    }
}
