package id.co.iconpln.controlflowapp.backgroundThread

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_background_thread.*
import kotlinx.coroutines.*
import java.lang.Runnable
import java.lang.ref.WeakReference
import java.net.URL

class BackgroundThreadActivity : AppCompatActivity(), View.OnClickListener, ContactAsyncTaskCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_background_thread)

        setClickListener()
    }

    private fun setClickListener() {
        btnThreadWorker.setOnClickListener(this)
        btnThreadHandler.setOnClickListener(this)
        btnThreadAsyncTask.setOnClickListener(this)
        btnThreadCoroutine.setOnClickListener(this)
        btnThreadCoroutineAsync.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnThreadWorker -> threadWorker()
            R.id.btnThreadHandler -> threadHandler()
            R.id.btnThreadAsyncTask -> threadAsyncTask()
            R.id.btnThreadCoroutine -> threadCoroutine()
            R.id.btnThreadCoroutineAsync -> threadCoroutineAsync()
        }
    }

    private fun threadWorker() {
        /*
                Don't Call Network in Main Thread

                    val text = URL(
                        "https://api.androidhive.info/contacts/"
                    ).readText()

                    tvThreadWorkerResult.text = text
                 */

        Thread(Runnable {
            val contactResultText = URL(
                "https://api.androidhive.info/contacts/"
            ).readText()
            /*
            Don't Call UI thread in background

                tvThreadWorkerResult.text = contactResultText
             */

            tvThreadWorkerResult.post(Runnable {
                tvThreadWorkerResult.text = contactResultText
            })
        }).start()
    }

    private fun threadHandler() {
        Thread(Runnable {
            val contactResultText = URL(
                "https://api.androidhive.info/contacts/"
            ).readText()
            val msg = Message.obtain()
            with(msg) {
                obj = contactResultText
                target = contactHandler
                sendToTarget()
            }
        }).start()
    }

    private val contactHandler = Handler { message ->
        tvThreadHandlerResult.text = message.obj as String
        true
    }

    private fun threadAsyncTask() {
        val contacUrl = URL("https://api.androidhive.info/contacts/")
        FetchContactAsyncTask(this).execute(contacUrl)
    }

    override fun onPreExecute() {
        pbThreadAsyncProgress.visibility = View.VISIBLE
        tvThreadAsyncResult.visibility = View.GONE
    }

    override fun onProgressUpdate(vararg values: Int?) {

    }

    override fun onPostExecute(result: String?) {
        pbThreadAsyncProgress.visibility = View.GONE
        tvThreadAsyncResult.visibility = View.VISIBLE
        tvThreadAsyncResult.text = result
    }

    class FetchContactAsyncTask(val listener: ContactAsyncTaskCallback):
        AsyncTask<URL, Int, String>() {

        // Using Weak Reference to avoid memory leak in Async task
        private val contactListener: WeakReference<ContactAsyncTaskCallback> =
            WeakReference(listener)

        override fun onPreExecute() {
            super.onPreExecute()
            val myListener = contactListener.get()
            myListener?.onPreExecute()
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            val myListener = contactListener.get()
            myListener?.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            val myListener = contactListener.get()
            myListener?.onPostExecute(result)
        }

        override fun doInBackground(vararg urls: URL): String {
            val urlResult = urls[0].readText()
            return urlResult
        }

    }

    private fun threadCoroutine() {
        runBlocking {
            launch {
                delay(1000)
                tvThreadCoroutineResult.text = "Coroutine!"
            }
        }
    }

    suspend fun getNumber(): Int {
        delay(1000)
        return 3 * 2
    }

    private fun threadCoroutineAsync() {
//        runBlocking {
//            val numberAsync = async { getNumber() }
//            val result = numberAsync.await()
//            tvThreadCoroutineAsyncResult.text = result.toString()
//        }
        runBlocking {
            val contactAsync = async { getContact() }
            val contacts = contactAsync.await()
            tvThreadCoroutineAsyncResult.text = contacts
        }
    }

    suspend fun getContact(): String {
        return withContext(Dispatchers.IO) {
            URL("https://api.androidhive.info/contacts/").readText()
        }
    }


}

interface ContactAsyncTaskCallback {
    fun onPreExecute()
    fun onProgressUpdate(vararg values: Int?)
    fun onPostExecute(result: String?)
}