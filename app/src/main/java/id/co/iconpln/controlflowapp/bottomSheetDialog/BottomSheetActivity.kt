package id.co.iconpln.controlflowapp.bottomSheetDialog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_bottom_sheet.view.*
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_content_main.*
import kotlinx.android.synthetic.main.layout_content_main.tvBottomActivity

class BottomSheetActivity : AppCompatActivity(), View.OnClickListener,
    BottomSheetFragment.ItemClickListener {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        setupActionBar()
        setOnClickButton()
        setupBottomSheetBehavior()
    }

    private fun setupBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(view: View, slideOffset: Float) {

            }

            override fun onStateChanged(view: View, newState: Int) {
                when(newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        btnBottomSheet.text = "Expand Bottom Sheet"
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {}
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        btnBottomSheet.text = "Close Bottom Sheet"
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {}
                    BottomSheetBehavior.STATE_SETTLING -> {}
                    else -> {}
                }
            }

        })
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbarBottomSheet)
    }

    private fun setOnClickButton() {
        btnBottomSheet.setOnClickListener(this)
        btnBottomSheetDialog.setOnClickListener(this)
        btnBottomSheetDialogFragment.setOnClickListener(this)
        btnBottomPayment.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.btnBottomSheet -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    btnBottomSheet.text = "Close Bottom Sheet"
                } else {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                    btnBottomSheet.text = "Expand Bottom Sheet"
                }
            }
            R.id.btnBottomSheetDialog -> {
                val dialogView = layoutInflater.inflate(R.layout.fragment_bottom_sheet, null)

                setDialogListener(dialogView)
                val bottomSheetDialog = BottomSheetDialog(this)
                bottomSheetDialog.setContentView(dialogView)
                bottomSheetDialog.show()
            }
            R.id.btnBottomSheetDialogFragment -> {
                val bottomSheetFragment = BottomSheetFragment()
                bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
            }
            R.id.btnBottomPayment -> {
                tvBottomActivity.text = "Payment"
            }
        }
    }

    private fun setDialogListener(dv: View?) {

        setDialogTextView(dv?.llBottomPreview, dv?.tvBottomPreview)
        setDialogTextView(dv?.llBottomShare, dv?.tvBottomShare)
        setDialogTextView(dv?.llBottomEdit, dv?.tvBottomEdit)
        setDialogTextView(dv?.llBottomSearch, dv?.tvBottomSearch)
        setDialogTextView(dv?.llBottomExit, dv?.tvBottomExit)

    }

    private fun setDialogTextView(linearLayout: LinearLayout?, textView: TextView?) {
        linearLayout?.setOnClickListener {
            val text = textView?.text.toString()
            onItemClick(getString(R.string.dialog_format, text))
        }
    }

    override fun onItemClick(text: String) {
        tvBottomActivity.text = text
    }
}
