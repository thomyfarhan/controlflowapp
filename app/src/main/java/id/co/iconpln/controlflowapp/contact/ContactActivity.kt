package id.co.iconpln.controlflowapp.contact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_contact.*

class ContactActivity : AppCompatActivity() {

    private lateinit var contactViewModel: ContactViewModel
    private lateinit var adapter: ContactAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        initViewModel()
        initRecyclerView()
        setContactData()
        fetchContactData()
    }

    private fun initViewModel() {
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel::class.java)
    }

    private fun initRecyclerView() {
        adapter = ContactAdapter()

        rvContactList.layoutManager = LinearLayoutManager(this)
        rvContactList.adapter = adapter
    }

    private fun setContactData() {
        contactViewModel.setContacts()
    }

    private fun fetchContactData() {
        contactViewModel.getContacts().observe(this, Observer { contacts ->
            contacts?.let {
                adapter.setContactData(it)
            }
        })
    }


}
