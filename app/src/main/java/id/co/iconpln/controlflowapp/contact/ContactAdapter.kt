package id.co.iconpln.controlflowapp.contact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.item_list_contact.view.*

class ContactAdapter: RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    private val contacts = ArrayList<Contact>()

    fun setContactData(contactData: ArrayList<Contact>) {
        contacts.clear()
        contacts.addAll(contactData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_contact, parent, false)

        return ContactViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(contacts[position])
    }

    inner class ContactViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(contact: Contact) {
            itemView.tvContactName.text = contact.nama
            itemView.tvContactEmail.text = contact.email
            itemView.tvContactMobile.text = contact.mobile
        }
    }
}