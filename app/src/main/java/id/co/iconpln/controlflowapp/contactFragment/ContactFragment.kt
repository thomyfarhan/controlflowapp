package id.co.iconpln.controlflowapp.contactFragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_contact.*

class ContactFragment : Fragment() {

    private lateinit var viewModel: ContactFragmentViewModel
    private lateinit var adapter: ContactFragmentAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        initViewModel()
        setContactList()
        fetchContactList()

        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showContactList()
    }

    private fun showContactList() {
        adapter = ContactFragmentAdapter()

        rvContactFrag.layoutManager = LinearLayoutManager(requireContext())
        rvContactFrag.adapter = adapter
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(ContactFragmentViewModel::class.java)
    }

    private fun setContactList() {
        viewModel.setContacts()
    }

    private fun fetchContactList() {
        viewModel.getContacts().observe(this, Observer {
            it?.let {
                adapter.setContactList(it)
                pbContactFrag.visibility = View.GONE
                rvContactFrag.visibility = View.VISIBLE
            }
        })
    }


}
