package id.co.iconpln.controlflowapp.contactFragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.contact.Contact
import kotlinx.android.synthetic.main.item_list_contact.view.*

class ContactFragmentAdapter:
    RecyclerView.Adapter<ContactFragmentAdapter.ContactFragmentViewHolder>() {

    private val contacts = ArrayList<Contact>()

    fun setContactList(itemList: ArrayList<Contact>) {
        contacts.clear()
        contacts.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactFragmentViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_contact, parent, false)

        return ContactFragmentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: ContactFragmentViewHolder, position: Int) {
        holder.bind(contacts[position])
    }

    inner class ContactFragmentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(contact: Contact) {
            itemView.tvContactName.text = contact.nama
            itemView.tvContactEmail.text = contact.email
            itemView.tvContactMobile.text = contact.mobile
        }
    }
}