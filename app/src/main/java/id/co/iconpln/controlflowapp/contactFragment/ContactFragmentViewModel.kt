package id.co.iconpln.controlflowapp.contactFragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import id.co.iconpln.controlflowapp.contact.Contact
import org.json.JSONObject
import java.lang.Exception

class ContactFragmentViewModel: ViewModel() {

    private val listContacts = MutableLiveData<ArrayList<Contact>>()

    fun setContacts() {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Contact>()
        val url = "https://api.androidhive.info/contacts/"

        client.get(url, object: AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray) {

                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)

                    val contacts = responseObject.getJSONArray("contacts")

                    for (i in 0 until contacts.length()) {
                        val contact = contacts.getJSONObject(i)
                        val contactItem = Contact()

                        with(contactItem) {
                            id = contact.getString("id")
                            nama = contact.getString("name")
                            email = contact.getString("email")
                            mobile = contact.getJSONObject("phone").getString("mobile")
                        }

                        listItems.add(contactItem)
                    }

                    listContacts.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("onFailure", e.message.toString())
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable) {

                Log.d("onFailure", error.message.toString())
            }

        })
    }

    fun getContacts(): LiveData<ArrayList<Contact>> {
        return listContacts
    }
}