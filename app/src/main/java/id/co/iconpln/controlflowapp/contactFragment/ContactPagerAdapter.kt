package id.co.iconpln.controlflowapp.contactFragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.co.iconpln.controlflowapp.fragmentTab.SecondFragment

class ContactPagerAdapter(private val fm: FragmentManager)
    : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    companion object {
        val TABTITLES = arrayOf("Contact", "Second")
    }

    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> ContactFragment()
            1 -> SecondFragment()
            else -> null
        } as Fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TABTITLES[position]
    }
}