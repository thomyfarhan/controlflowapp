package id.co.iconpln.controlflowapp.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FavoriteDatabaseDao {

    @Insert
    fun insertUser(user: FavoriteUser)

    @Query("DELETE FROM fav_user_table WHERE userId = :key")
    fun deleteUser(key: Long)

    @Query("SELECT * FROM fav_user_table ORDER BY favUserId DESC")
    fun selectAll(): LiveData<List<FavoriteUser>>

    @Query("SELECT * FROM fav_user_table WHERE userId = :key")
    fun getFavUser(key: Long): LiveData<FavoriteUser>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(user: FavoriteUser)
}