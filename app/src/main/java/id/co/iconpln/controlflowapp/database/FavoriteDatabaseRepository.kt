package id.co.iconpln.controlflowapp.database

import androidx.lifecycle.LiveData
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse

class FavoriteDatabaseRepository(private val favoriteDatabaseDao: FavoriteDatabaseDao) {

    val allFavUsers: LiveData<List<FavoriteUser>> = favoriteDatabaseDao.selectAll()

    fun insertUser(user: FavoriteUser) {
        favoriteDatabaseDao.insertUser(user)
    }

    fun deleteUser(id: Long) {
        favoriteDatabaseDao.deleteUser(id)
    }

    fun getUser(id: Long): LiveData<FavoriteUser> {
        return favoriteDatabaseDao.getFavUser(id)
    }

    fun updateUser(user: FavoriteUser) {
        favoriteDatabaseDao.update(user)
    }

    fun refreshList(dbList: List<FavoriteUser>, networkList: List<UserDataResponse>) {
        next@ for (dbUser in dbList) {
            for (networkUser in networkList) {
                if (dbUser.userId.toInt() == networkUser.id) {
                    with(dbUser) {
                        userAddress = networkUser.address
                        userName = networkUser.name
                        userPhone = networkUser.phone
                    }
                    favoriteDatabaseDao.update(dbUser)
                    continue@next
                }
            }

            favoriteDatabaseDao.deleteUser(dbUser.userId.toLong())
        }
    }
}
