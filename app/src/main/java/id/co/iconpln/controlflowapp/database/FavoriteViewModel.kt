package id.co.iconpln.controlflowapp.database

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.network.MyUserNetworkRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavoriteViewModel(app: Application): AndroidViewModel(app) {

    private val repository: FavoriteDatabaseRepository

    private val allFavoriteUsers: LiveData<List<FavoriteUser>>

    fun getListUsers(): MutableLiveData<ArrayList<UserDataResponse>> {
        return MyUserNetworkRepository().getUsers()
    }

    init {
        val favDatabaseDao = FavoriteDatabase.getInstance(app).favoriteDatabaseDao
        repository = FavoriteDatabaseRepository(favDatabaseDao)
        allFavoriteUsers = repository.allFavUsers
    }

    fun getAllFavoriteUsers(): LiveData<List<FavoriteUser>> {
        return allFavoriteUsers
    }

    fun insertUser(user: FavoriteUser) {
        GlobalScope.launch {
            repository.insertUser(user)
            Log.d("Room Database", "User Name : ${user.userName}, Inserted.")
        }
    }

    fun deleteUser(id: Long) {
        GlobalScope.launch {
            repository.deleteUser(id)
            Log.d("Room Database", "User Id: $id Deleted")
        }
    }

    fun getUser(id: Long): LiveData<FavoriteUser> {
        return repository.getUser(id)
    }

    fun updateUser(user: FavoriteUser) {
        GlobalScope.launch {
            repository.updateUser(user)
            Log.d("Room Database", "User Id: ${user.userId} Update")
        }
    }

    fun refreshList(networkUsers: List<UserDataResponse>) {
        GlobalScope.launch {
            repository.refreshList(
                getAllFavoriteUsers().value as List<FavoriteUser>,
                networkUsers
            )
        }
    }
}