package id.co.iconpln.controlflowapp.fragmentNavDrawer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.StyleActivity
import id.co.iconpln.controlflowapp.fragmentTab.FirstFragment
import id.co.iconpln.controlflowapp.fragmentTab.SecondFragment
import id.co.iconpln.controlflowapp.hero.ListHeroFragment
import kotlinx.android.synthetic.main.activity_nav_drawer.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class NavDrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_drawer)

        setupActionBar()
        navViewDrawer.setNavigationItemSelectedListener(this)
        selectFirstNavigationMenu()
    }

    private fun selectFirstNavigationMenu() {
        navViewDrawer.menu.performIdentifierAction(R.id.nav_home, 0)
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, 0)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_home -> {
                loadFragment(FirstFragment())
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_profile -> {
                loadFragment(ListHeroFragment())
            }
            R.id.nav_gallery -> {
                startActivity(Intent(this, StyleActivity::class.java))
                Toast.makeText(this, "Gallery", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_edit -> {
                loadFragment(SecondFragment())
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_exit -> {
                finish()
            }
        }

        uncheckItemMenu()

        item.isChecked = true
        title = item.title

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun uncheckItemMenu() {
        for (menu in 0 until navViewDrawer.menu.size()) {
            navViewDrawer.menu.getItem(menu).isChecked = false

            if (navViewDrawer.menu.getItem(menu).hasSubMenu()) {
                for (submenu in 0 until
                        navViewDrawer.menu.getItem(menu).subMenu.size()) {

                    navViewDrawer.menu.getItem(menu).
                        subMenu.getItem(submenu).isChecked = false
                }
            }
        }
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flDrawerContainer, fragment, fragment::class.java.simpleName)
            .commit()
    }
}
