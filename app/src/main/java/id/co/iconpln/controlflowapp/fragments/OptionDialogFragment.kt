package id.co.iconpln.controlflowapp.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_option_dialog.*

class OptionDialogFragment : DialogFragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_option_dialog, container, false)
    }

    private var optionDialogListener: OnOptionsDialogListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnDialogChoose.setOnClickListener(this)
        btnDialogClose.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnDialogChoose -> {
                val checkedRadioButton = rgDialogOptions.checkedRadioButtonId
                if(checkedRadioButton != -1) {
                    val favColor = when(checkedRadioButton) {
                        R.id.rbDialogBlue -> rbDialogBlue.text.toString().trim()
                        R.id.rbDialogRed -> rbDialogRed.text.toString().trim()
                        R.id.rbDialogPurple -> rbDialogPurple.text.toString().trim()
                        R.id.rbDialogGreen -> rbDialogGreen.text.toString().trim()
                        else -> ""
                    }
//                    Toast.makeText(requireContext(), favColor, Toast.LENGTH_SHORT).show()

                    if(optionDialogListener != null) {
                        optionDialogListener?.onOptionsChosen(favColor)
                    }
                    requireDialog().dismiss()
                }
            }
            R.id.btnDialogClose -> {
                requireDialog().cancel()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val parentFragment = parentFragment
        if(parentFragment is LastFragment) {
            val lastFragment = parentFragment
            this.optionDialogListener = lastFragment.optionDialogListener
        }
    }

    override fun onDetach() {
        super.onDetach()

        this.optionDialogListener = null
    }

    interface OnOptionsDialogListener {
        fun onOptionsChosen(text: String)
    }
}
