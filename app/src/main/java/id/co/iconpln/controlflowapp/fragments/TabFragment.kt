package id.co.iconpln.controlflowapp.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity

import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.fragmentTab.TabPagerAdapter
import kotlinx.android.synthetic.main.fragment_tab.*

class TabFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTab()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.elevation = 0f
    }

    private fun setupTab() {
        val tabPagerAdapter = TabPagerAdapter(requireContext(), childFragmentManager)
        vpFrag.adapter = tabPagerAdapter
        
        tabFrag.setupWithViewPager(vpFrag)
    }
}
