package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*

class GridHeroActivity : AppCompatActivity() {

    private val rvGridHero: RecyclerView
        get() = rv_grid_hero

    private lateinit var listHero: MutableList<Hero>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setUpListHero()
        showRecyclerGrid()
    }

    private fun setUpListHero() {
        rvGridHero.setHasFixedSize(true)
        listHero = HeroesData.listHeroes
        setupListDivider()
    }

    private fun setupListDivider() {
        val dividerItemDecoration = DividerItemDecoration(
            rvGridHero.context, DividerItemDecoration.VERTICAL)

        rvGridHero.addItemDecoration(dividerItemDecoration)
    }

    private fun showRecyclerGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 3)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvGridHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnClickGridItemCallback(
            object: GridHeroAdapter.OnClickGridItemCallback {

                override fun onClick(hero: Hero) {
                    Toast.makeText(this@GridHeroActivity, hero.name, Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }
}
