package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import kotlinx.android.synthetic.main.activity_list_hero.*

class ListHeroActivity : AppCompatActivity() {

    companion object {
        private const val STATE_LIST = "state_list"
        private const val STATE_TITLE = "state_title"
        private const val STATE_MODE = "state_mode"
    }

    private val rvListHero: RecyclerView
        get() = rv_list_hero

    private lateinit var listHero: ArrayList<Hero>
    private var title: String = "Mode List"
    private var mode: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_hero)

        if (savedInstanceState == null) {
            setUpListHero(getDataHeroes())
            mode = R.id.action_hero_list
            setActionBarTitle(title)
            showRecyclerList()
        } else {
            title = savedInstanceState.getString(STATE_TITLE).toString()
            val stateList = savedInstanceState.getParcelableArrayList<Hero>(STATE_LIST)
            val stateMode = savedInstanceState.getInt(STATE_MODE)

            val heroes = ArrayList<Hero>()
            stateList?.let {
                heroes.addAll(it)
            }
            setUpListHero(heroes)

            setActionBarTitle(title)
            setListMode(stateMode)

        }


    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_TITLE, title)
        outState.putInt(STATE_MODE, mode)
        outState.putParcelableArrayList(STATE_LIST, listHero)
    }

    private fun setActionBarTitle(title: String) {
        supportActionBar?.title = title
    }

    private fun getDataHeroes(): ArrayList<Hero> {
        val heroName = resources.getStringArray(R.array.hero_name)
        val heroDesc = resources.getStringArray(R.array.hero_description)
        val heroPhoto = resources.getStringArray(R.array.hero_photo)

        val heroes = arrayListOf<Hero>()
        for (position in heroName.indices) {
            val hero = Hero(
                heroName[position],
                heroDesc[position],
                heroPhoto[position]
            )
            heroes.add(hero)
        }
        return heroes
    }

    private fun showRecyclerList() {
        rvListHero.layoutManager = LinearLayoutManager(this)
        val listHeroAdapter = ListHeroAdapter(listHero)
        rvListHero.adapter = listHeroAdapter

        listHeroAdapter.setOnItemClickCallback(object: ListHeroAdapter.OnItemClickCallback {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@ListHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun setUpListHero(list: ArrayList<Hero>) {
        rvListHero.setHasFixedSize(true)
        listHero = list
        setupListDivider()
    }

    private fun setupListDivider() {
        val dividerItemDecoration = DividerItemDecoration(
            rvListHero.context, DividerItemDecoration.VERTICAL)

        rvListHero.addItemDecoration(dividerItemDecoration)
    }

    private fun showRecyclerGrid() {
        rvListHero.layoutManager = GridLayoutManager(this, 3)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvListHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnClickGridItemCallback(
            object: GridHeroAdapter.OnClickGridItemCallback {

                override fun onClick(hero: Hero) {
                    Toast.makeText(this@ListHeroActivity, hero.name, Toast.LENGTH_SHORT)
                        .show()
                }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_hero, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setListMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setListMode(selectedMode: Int) {
        when(selectedMode) {
            R.id.action_hero_grid -> {
                title = "Mode Grid"
                showRecyclerGrid()
            }
            R.id.action_hero_list -> {
                title = "Mode List"
                showRecyclerList()
            }
        }
        setActionBarTitle(title)
        mode = selectedMode
    }
}
