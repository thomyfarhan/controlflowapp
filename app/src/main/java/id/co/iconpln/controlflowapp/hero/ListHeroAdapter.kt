package id.co.iconpln.controlflowapp.hero

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import kotlinx.android.synthetic.main.item_list_hero.view.*

/**
 * Adapter needs viewHolder to access view items only once for all list
 * this class need to extends RecyclerView.Adapter with ViewHolder on it
 *
 * @param listHero: holds heroes data
 */
class ListHeroAdapter(val listHero: MutableList<Hero>): RecyclerView.Adapter<ListHeroAdapter.HeroViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    /**
     * Return inital create of view
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_hero, parent, false)
        return HeroViewHolder(view)
    }

    /**
     * Return size of list data
     */
    override fun getItemCount(): Int {
        return listHero.size
    }

    /**
     * Bind the view that we already create on onCreateViewHolder function
     * every items in the list need to run this function
     */
    override fun onBindViewHolder(holder: HeroViewHolder, position: Int) {
        holder.bind(listHero[position])
        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClick(listHero[position])
        }
    }

    /**
     * Nested class using inner to enabling access parent attribute
     *
     * @param view: holds all view items
     */
    inner class HeroViewHolder (
        private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(hero: Hero) {
            view.tvHeroName.text = hero.name
            view.tvHeroDesc.text = hero.desc

            Glide.with(view.context)
                .load(hero.photo)
                .into(view.ivHeroImage)
        }
    }

    /**
     * Set onItemClickCallback to the new object created by activity
     *
     * @param onItemClickCallback: new object that has operations
     */
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    /**
     * Object for accommodating function to be called
     */
    interface OnItemClickCallback {
        fun onItemClick(hero: Hero)
    }

}
