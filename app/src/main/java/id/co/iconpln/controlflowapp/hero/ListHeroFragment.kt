package id.co.iconpln.controlflowapp.hero


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.fragment_list_hero.*

class ListHeroFragment : Fragment() {

    private lateinit var listHero: MutableList<Hero>
    private lateinit var listAdapter: ListHeroAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_list_hero, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpListHero()
        showRecyclerList()
        setListItemClickListener()
    }

    private fun setListItemClickListener() {
        listAdapter.setOnItemClickCallback(object: ListHeroAdapter.OnItemClickCallback{
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@ListHeroFragment.requireContext(),
                    hero.name, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun showRecyclerList() {
        rvListHeroFrag.layoutManager = LinearLayoutManager(requireContext())
        listAdapter = ListHeroAdapter(listHero)
        rvListHeroFrag.adapter = listAdapter
    }

    private fun setUpListHero() {
        rvListHeroFrag.setHasFixedSize(true)
        listHero = HeroesData.listHeroes
        setupListDivider()
    }

    private fun setupListDivider() {
        val dividerItemDecoration = DividerItemDecoration(
            rvListHeroFrag.context, DividerItemDecoration.VERTICAL)

        rvListHeroFrag.addItemDecoration(dividerItemDecoration)
    }

}
