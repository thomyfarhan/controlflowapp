package id.co.iconpln.controlflowapp.model.myUser

data class BaseUserResponse(
    val count: Int,
    val data: List<UserDataResponse>,
    val total: Int
)