package id.co.iconpln.controlflowapp.model.myUser

data class CreatedUserResponse(
    val created_users: UserDataResponse,
    val message: String
)