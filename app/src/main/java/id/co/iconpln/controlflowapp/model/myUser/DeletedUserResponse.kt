package id.co.iconpln.controlflowapp.model.myUser

data class DeletedUserResponse(
    val deleted_user: UserDataResponse,
    val message: String
)