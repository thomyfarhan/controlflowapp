package id.co.iconpln.controlflowapp.model.myUser

data class SingleUserResponse(
    val data: UserDataResponse
)