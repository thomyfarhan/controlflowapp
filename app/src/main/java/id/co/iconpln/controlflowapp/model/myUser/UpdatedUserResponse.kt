package id.co.iconpln.controlflowapp.model.myUser

data class UpdatedUserResponse(
    val message: String,
    val updated_user: UserDataResponse
)