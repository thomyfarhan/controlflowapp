package id.co.iconpln.controlflowapp.myContact

data class MyContact(
    var id: String = "0",
    var nama: String? = null,
    var email: String? = null,
    var mobile: String? = null
)