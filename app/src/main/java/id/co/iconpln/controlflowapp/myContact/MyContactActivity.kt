package id.co.iconpln.controlflowapp.myContact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_my_contact.*

class MyContactActivity : AppCompatActivity() {

    private lateinit var adapter: MyContactAdapter
    private lateinit var viewModel: MyContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_contact)

        initRecyclerView()
        initViewModel()
        fetchContactList()
    }

    private fun initRecyclerView() {
        adapter = MyContactAdapter()

        rvMyContactList.layoutManager = LinearLayoutManager(this)
        rvMyContactList.adapter = adapter
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this)
            .get(MyContactViewModel::class.java)
    }

    private fun fetchContactList() {
        viewModel.getListContacts().observe(this, Observer { myContacts ->
            myContacts?.let {
                adapter.setContactList(it)
            }
        })
    }
}
