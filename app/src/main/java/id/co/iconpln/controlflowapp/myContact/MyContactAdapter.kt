package id.co.iconpln.controlflowapp.myContact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myContact.ContactResponse
import kotlinx.android.synthetic.main.item_list_mycontact.view.*

class MyContactAdapter: RecyclerView.Adapter<MyContactAdapter.MyContactViewHolder>() {

    private val myContacts = ArrayList<ContactResponse>()

    fun setContactList(itemList: ArrayList<ContactResponse>) {
        myContacts.clear()
        myContacts.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyContactViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_mycontact, parent, false)
        return MyContactViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myContacts.size
    }

    override fun onBindViewHolder(holder: MyContactViewHolder, position: Int) {
        holder.bind(myContacts[position])
    }

    inner class MyContactViewHolder(itemView: View)
        : RecyclerView.ViewHolder(itemView) {

        fun bind(myContact: ContactResponse) {
            itemView.tvMyContactName.text = myContact.name
            itemView.tvMyContactEmail.text = myContact.email
            itemView.tvMyContactMobile.text = myContact.phone.mobile
        }
    }
}