package id.co.iconpln.controlflowapp.myProfile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileUser
import id.co.iconpln.controlflowapp.profileLogin.ProfileLoginActivity
import kotlinx.android.synthetic.main.activity_my_profile.*

class MyProfileActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val REQUEST_CODE = 200
    }

    private lateinit var viewModel: MyProfileViewModel

    private var profileLoginResponse: ProfileLoginResponse? = null
    private lateinit var profileUserPreference: ProfileUserPreference
    private lateinit var profileUser: ProfileUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        initViewModel()
        setClickListener()
        showExistingPreference()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MyProfileViewModel::class.java)
    }

    private fun setClickListener() {
        btnProfileToLogin.setOnClickListener(this)
        btnProfileLogout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnProfileToLogin -> {
                val loginIntent = Intent(this, ProfileLoginActivity::class.java)
                startActivityForResult(loginIntent, REQUEST_CODE)
            }
            R.id.btnProfileLogout -> {
                profileUserPreference.removeProfileUser(profileUser)
                showLogoutProfile()
            }
        }
    }

    private fun showExistingPreference() {
        profileUserPreference = ProfileUserPreference(this)
        profileUser = profileUserPreference.getProfileUser()

        val token = profileUserPreference.getProfileUser().userToken
        if (!token.isNullOrEmpty()) {
            initiateGetProfile(token)
        } else {
            showLogoutProfile()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == ProfileLoginActivity.RESULT_CODE) {
                profileLoginResponse = data?.getParcelableExtra(
                    ProfileLoginActivity.EXTRA_PROFILE_RESULT
                ) as ProfileLoginResponse
                saveProfileUserPreference()
            }
        }
    }

    private fun saveProfileUserPreference() {
        if (profileLoginResponse != null) {
            profileUser.userToken = profileLoginResponse?.token
            profileUserPreference.setProfileUser(profileUser)
            Toast.makeText(this, "${profileUser.userToken}", Toast.LENGTH_SHORT).show()
            showExistingPreference()
        }
    }

    private fun showLogoutProfile() {
        pbProfileLoading.visibility = View.GONE
        llProfileLoading.visibility = View.VISIBLE
        tvProfileWarning.visibility = View.VISIBLE
        btnProfileToLogin.visibility = View.VISIBLE
        btnProfileLogout.visibility = View.GONE

        tvProfileId.text = resources.getString(R.string.empty_text)
        tvProfileName.text = resources.getString(R.string.empty_text)
        tvProfileEmail.text = resources.getString(R.string.empty_text)
        tvProfileHandphone.text = resources.getString(R.string.empty_text)
    }

    private fun fetchUserProfile(token: String) {
        viewModel.getProfile(token).observe(this, Observer {
            if (it != null) {
                showProfile(it)
            } else {
                Toast.makeText(this, "Failed to get Profile", Toast.LENGTH_SHORT).show()
                profileUserPreference.removeProfileUser(profileUser)
                showLogoutProfile()
            }
        })
    }

    private fun showProfile(profileResponse: ProfileResponse) {
        pbProfileLoading.visibility = View.GONE
        llProfileLoading.visibility = View.VISIBLE
        tvProfileWarning.visibility = View.GONE
        btnProfileToLogin.visibility = View.GONE
        btnProfileLogout.visibility = View.VISIBLE

        tvProfileId.text = profileResponse.id.toString()
        tvProfileName.text = profileResponse.name
        tvProfileEmail.text = profileResponse.email
        tvProfileHandphone.text = profileResponse.phone
    }

    private fun initiateGetProfile(token: String) {
        pbProfileLoading.visibility = View.VISIBLE
        llProfileLoading.visibility = View.GONE
        fetchUserProfile(token)
    }

}
