package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var viewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        setClickButton()
        initRecyclerView()
        initViewModel()
        fetchUserList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_my_user, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_favorite_list -> {
                startActivity(Intent(this, MyUserFavoriteActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRecyclerView() {
        adapter = MyUserAdapter(object: MyUserListener {
            override fun onClick(user: UserDataResponse) {
                openUserForm(user, true)
            }
        })

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this)
            .get(MyUserViewModel::class.java)
    }

    private fun fetchUserList() {
        viewModel.getListUsers().observe(this, Observer {
            if (it != null) {
                adapter.setMyUserList(it)
                rvMyUserList.visibility = View.VISIBLE
                fabMyUserAdd.show()
            } else {
                Toast.makeText(this, "No Internet Connection!", Toast.LENGTH_SHORT).show()
            }
            pbMyUser.visibility = View.GONE
        })
    }

    private fun setClickButton() {
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.fabMyUserAdd -> {
                val defaultUser = UserDataResponse("", 0, "", "")
                openUserForm(defaultUser, false)
            }
        }
    }

    private fun openUserForm(user: UserDataResponse, isEditUser: Boolean) {
        val intent = Intent(applicationContext, MyUserFormActivity::class.java)
        intent.putExtra(MyUserFormActivity.EXTRA_USER_ID, user.id)
        intent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, isEditUser)
        startActivity(intent)
    }

    override fun onPause() {
        super.onPause()
        rvMyUserList.visibility = View.GONE
        fabMyUserAdd.hide()
        pbMyUser.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        fetchUserList()
    }
}
