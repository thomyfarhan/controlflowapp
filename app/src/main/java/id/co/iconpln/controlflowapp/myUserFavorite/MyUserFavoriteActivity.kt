package id.co.iconpln.controlflowapp.myUserFavorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user_favorite.*

class MyUserFavoriteActivity : AppCompatActivity() {

    private lateinit var viewModel: FavoriteViewModel
    private lateinit var adapter: MyUserFavoriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_favorite)

        initViewModel()
        initRecyclerView()
        fetchFavoriteData()
        addListClickListener()
        refreshListItem()
    }

    private fun refreshListItem() {
        viewModel.getListUsers().observe(this, Observer {
            if (it != null) {
                viewModel.refreshList(it)
            }
            pbMyUserFavorite.visibility = View.GONE
            rvMyUserFavoriteList.visibility = View.VISIBLE
        })
    }

    private fun fetchFavoriteData() {
        viewModel.getAllFavoriteUsers().observe(this, Observer {
            adapter.setFavItem(it)
        })
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application))
            .get(FavoriteViewModel::class.java)
    }

    private fun initRecyclerView() {
        adapter = MyUserFavoriteAdapter()

        rvMyUserFavoriteList.layoutManager = LinearLayoutManager(this)
        rvMyUserFavoriteList.adapter = adapter
    }

    private fun addListClickListener() {
        adapter.setClickListener(object: MyUserFavoriteListener {
            override fun onClick(user: FavoriteUser) {
                openUserForm(user)
            }

        })
    }

    private fun openUserForm(user: FavoriteUser) {
        val intent = Intent(applicationContext, MyUserFormActivity::class.java)
        intent.putExtra(MyUserFormActivity.EXTRA_USER_ID, user.userId.toInt())
        intent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        refreshListItem()
    }
}
