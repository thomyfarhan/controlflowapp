package id.co.iconpln.controlflowapp.myUserFavorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import kotlinx.android.synthetic.main.item_list_myuser.view.*

class MyUserFavoriteAdapter: RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserFavoriteViewHolder>() {

    private val favUsers = ArrayList<FavoriteUser>()
    private var myUserFavoriteListener: MyUserFavoriteListener? = null

    fun setFavItem(items: List<FavoriteUser>) {
        favUsers.clear()
        favUsers.addAll(items)
        notifyDataSetChanged()
    }

    fun setClickListener(clickListener: MyUserFavoriteListener) {
        this.myUserFavoriteListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserFavoriteViewHolder {
        return MyUserFavoriteViewHolder.with(parent)
    }

    override fun getItemCount(): Int {
        return favUsers.size
    }

    override fun onBindViewHolder(holder: MyUserFavoriteViewHolder, position: Int) {
        holder.bind(favUsers[position], myUserFavoriteListener)
    }

    class MyUserFavoriteViewHolder(itemView: View)
        : RecyclerView.ViewHolder(itemView) {

        companion object {
            fun with(parent: ViewGroup): MyUserFavoriteViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_list_myuser, parent, false)

                return MyUserFavoriteViewHolder(view)
            }
        }

        fun bind(favUser: FavoriteUser, clickListener: MyUserFavoriteListener?) {
            itemView.tvMyUserName.text = favUser.userName
            itemView.tvMyUserAddress.text = favUser.userAddress
            itemView.tvMyUserMobile.text = favUser.userPhone
            itemView.setOnClickListener {
                clickListener?.onClick(favUser)
            }
        }
    }
}

interface MyUserFavoriteListener {
    fun onClick(user: FavoriteUser)
}