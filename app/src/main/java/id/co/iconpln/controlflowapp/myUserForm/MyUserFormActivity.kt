package id.co.iconpln.controlflowapp.myUserForm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER_EDIT = "user_edit"
        const val EXTRA_USER_ID = "user_id"
    }

    private lateinit var viewModel: MyUserFormViewModel
    private lateinit var favoriteViewModel: FavoriteViewModel
    private var userId: Int? = null
    private var isEditUser: Boolean = false

    private var menuItem: Menu? = null
    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        setClickButton()
        getIntentExtra()
        initViewModel()
        checkForm()
        viewModelObserver()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_my_user_form, menu)
        menuItem = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_favourite -> {
                isFavorite = if (!isFavorite) {
                    addToFavorite()
                    Toast.makeText(this, "Add to favorite!", Toast.LENGTH_SHORT).show()
                    true
                } else {
                    removeFromFavorite()
                    Toast.makeText(this, "Removed from favorite!", Toast.LENGTH_SHORT).show()
                    false
                }
                setFavoriteVisibility(isFavorite)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setFavoriteUser() {
        favoriteViewModel.getUser(userId!!.toLong()).observe(this, Observer {
            isFavorite = it != null
            setFavoriteVisibility(isFavorite)
        })
    }

    private fun addToFavorite() {
        val favUser = FavoriteUser(
            0L,
            etUserFormAddress.text.toString(),
            userId.toString(),
            etUserFormName.text.toString(),
            etUserFormHp.text.toString()
        )

        favoriteViewModel.insertUser(favUser)
    }

    private fun removeFromFavorite() {
        if (userId != 0) {
            favoriteViewModel.deleteUser(userId!!.toLong())
        }
    }

    private fun viewModelObserver() {
        favoriteViewModel.getAllFavoriteUsers().observe(this, Observer {

            if (it.isNotEmpty()) {
                for (i in it.indices) {
                    Log.d("Room Database Result", "${it[i].userId}, ${it[i].userName}")
                }
            }
        })
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.action_favourite).isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    private fun checkForm() {
        if (isEditUser) {
            btnUserFormAdd.visibility = View.GONE
            getUser(userId ?: 0)
        } else {
            btnUserFormDelete.visibility = View.GONE
            btnUserFormSave.visibility = View.GONE
            pbUserForm.visibility = View.GONE
            nsvMyUserFormContent.visibility = View.VISIBLE
        }
        invalidateOptionsMenu()
    }

    private fun setClickButton() {
        btnUserFormAdd.setOnClickListener(this)
        btnUserFormSave.setOnClickListener(this)
        btnUserFormDelete.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnUserFormAdd -> {
                val newUserData = UserDataResponse(
                    etUserFormAddress.text.toString(),
                    userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )

                pbUserForm.visibility = View.VISIBLE
                createUser(newUserData)
            }
            R.id.btnUserFormSave -> {
                userId?.let { id ->
                    val updateUserData = UserDataResponse(
                        etUserFormAddress.text.toString(),
                        id,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                    )

                    pbUserForm.visibility = View.VISIBLE
                    updateUser(id, updateUserData)
                }
            }
            R.id.btnUserFormDelete -> {
                userId?.let { id ->
                    pbUserForm.visibility = View.VISIBLE
                    deleteUser(id)
                }
            }
        }
    }

    private fun createUser(newUserData: UserDataResponse) {
        viewModel.createUser(newUserData).observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "Successfully Added!", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to add", Toast.LENGTH_SHORT).show()
            }
            pbUserForm.visibility = View.GONE
        })
    }

    private fun deleteUser(id: Int) {
        viewModel.deleteUser(id).observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "Successfully Deleted!", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to delete", Toast.LENGTH_SHORT).show()
            }
            pbUserForm.visibility = View.GONE
        })
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        viewModel.updateUser(id, userData).observe(this, Observer { userDataReponse ->

            if (userDataReponse != null) {
                Toast.makeText(this, "Updated Successfully", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to update", Toast.LENGTH_SHORT).show()
            }
            pbUserForm.visibility = View.GONE
        })
    }

    private fun getUser(id: Int) {
        viewModel.getUser(id).observe(this, Observer {

            if (it != null) {
                populateData(it)
                nsvMyUserFormContent.visibility = View.VISIBLE
                pbUserForm.visibility = View.GONE
                menuItem?.getItem(0)?.isVisible = true
                setFavoriteUser()

            } else {
                pbUserForm.visibility = View.GONE
                Toast.makeText(this, "Failed to get User data", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun getIntentExtra() {
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)
        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MyUserFormViewModel::class.java)

        favoriteViewModel = ViewModelProvider(this,
            ViewModelProvider.AndroidViewModelFactory(application))
            .get(FavoriteViewModel::class.java)
    }

    private fun populateData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)
    }

    private fun setFavoriteVisibility(visible: Boolean) {
        if (visible) {
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite)
        } else {
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
        }
    }


}
