package id.co.iconpln.controlflowapp.network

import androidx.lifecycle.MutableLiveData
import id.co.iconpln.controlflowapp.model.myContact.BaseContactResponse
import id.co.iconpln.controlflowapp.model.myContact.ContactResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyContactNetworkRepository {

    fun getContacts(): MutableLiveData<ArrayList<ContactResponse>> {
        val contactsData = MutableLiveData<ArrayList<ContactResponse>>()
        val listContacts = ArrayList<ContactResponse>()

        NetworkConfig.contactApi().fetchContacts().enqueue(
            object: Callback<BaseContactResponse<ContactResponse>> {
                override fun onResponse(
                    call: Call<BaseContactResponse<ContactResponse>>,
                    response: Response<BaseContactResponse<ContactResponse>>) {

                    if (response.isSuccessful) {
                        val listContactSize = response.body()?.contacts?.size as Int

                        for (i in 0 until listContactSize) {
                            listContacts.add(response.body()?.contacts?.get(i) as ContactResponse)
                        }
                        contactsData.postValue(listContacts)
                    }
                }

                override fun onFailure(
                    call: Call<BaseContactResponse<ContactResponse>>,
                    t: Throwable) {

                    contactsData.postValue(null)
                }
            }
        )
        return contactsData
    }
}