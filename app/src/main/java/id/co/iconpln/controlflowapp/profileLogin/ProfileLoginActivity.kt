package id.co.iconpln.controlflowapp.profileLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginUser
import id.co.iconpln.controlflowapp.profileRegister.ProfileRegisterActivity
import kotlinx.android.synthetic.main.activity_profile_login.*

class ProfileLoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_PROFILE_RESULT = "profile_result"
        const val RESULT_CODE = 201
    }

    private lateinit var viewModel: ProfileLoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_login)

        setOnClickListener()
        initViewModel()
    }

    private fun setOnClickListener() {
        btnProfileLogin.setOnClickListener(this)
        tvProfileLoginReg.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnProfileLogin -> {
                fetchUserData(
                    ProfileLoginUser(
                        etProfileLoginEmail.text.toString(),
                        etProfileLoginPassword.text.toString()
                    )
                )
            }
            R.id.tvProfileLoginReg -> {
                startActivity(Intent(this, ProfileRegisterActivity::class.java))
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(ProfileLoginViewModel::class.java)
    }

    private fun fetchUserData(profileLoginUser: ProfileLoginUser) {
        viewModel.login(profileLoginUser).observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "Success Login ${it.customer.email}",
                    Toast.LENGTH_SHORT).show()
                openProfilePage(it)
            } else {
                val errorMessage = ProfileLoginViewModel.errorMessage
                if (errorMessage.isEmpty()) {
                    Toast.makeText(this,
                        "Check your connection!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this,
                        "Login Failed $errorMessage", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun openProfilePage(profileLoginResponse: ProfileLoginResponse) {
        val resultIntent = Intent().putExtra(EXTRA_PROFILE_RESULT, profileLoginResponse)
        setResult(RESULT_CODE, resultIntent)
        finish()
    }
}
