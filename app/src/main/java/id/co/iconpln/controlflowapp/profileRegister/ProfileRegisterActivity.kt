package id.co.iconpln.controlflowapp.profileRegister

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileRegisterUser
import kotlinx.android.synthetic.main.activity_profile_register.*

class ProfileRegisterActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var viewModel: ProfileRegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_register)

        initViewModel()
        setOnClickViews()
    }

    private fun setOnClickViews() {
        btnProfileReg.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnProfileReg -> {
                val newUser = ProfileRegisterUser(
                    etProfileRegEmail.text.toString(),
                    etProfileRegPassword.text.toString(),
                    etProfileRegEmail.text.toString(),
                    etProfileRegHp.text.toString()
                )
                registerUser(newUser)
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(ProfileRegisterViewModel::class.java)
    }

    private fun registerUser(profileRegisterUser: ProfileRegisterUser) {
        viewModel.register(profileRegisterUser).observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "User Successfully created!", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to create new user!", Toast.LENGTH_SHORT).show()
            }
        })
    }


}
