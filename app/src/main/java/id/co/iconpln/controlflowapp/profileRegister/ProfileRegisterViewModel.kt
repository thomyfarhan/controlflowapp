package id.co.iconpln.controlflowapp.profileRegister

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myProfile.ProfileRegisterUser
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.network.MyProfileNetworkRepository

class ProfileRegisterViewModel: ViewModel() {

    fun register(profileRegisterUser: ProfileRegisterUser)
            : MutableLiveData<ProfileResponse> {

        return MyProfileNetworkRepository().doRegister(profileRegisterUser)
    }


}
