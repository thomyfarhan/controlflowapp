package id.co.iconpln.controlflowapp.sharedPreferences

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_shared_preferences.*

class SharedPreferencesActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val REQUEST_CODE = 100
    }

    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences)

        setOnClickButton()
        setUserPreferences()
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnPrefSave -> {
                val formType = if (isUserBlank(user)) {
                    SharedPreferencesFormActivity.TYPE_ADD
                } else {
                    SharedPreferencesFormActivity.TYPE_EDIT
                }

                val intent = Intent(this, SharedPreferencesFormActivity::class.java)
                intent.putExtra(SharedPreferencesFormActivity.EXTRA_USER, user)
                intent.putExtra(SharedPreferencesFormActivity.EXTRA_TYPE_FORM, formType)

                startActivityForResult(intent, REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == REQUEST_CODE) {
            if (resultCode == SharedPreferencesFormActivity.RESULT_CODE) {
                user = data?.getParcelableExtra(
                    SharedPreferencesFormActivity.EXTRA_RESULT) ?: User()
                setupTextView(user)
            }
        }
    }

    private fun setOnClickButton() {
        btnPrefSave.setOnClickListener(this)
    }

    private fun setUserPreferences() {
        val preferences = UserPreferences(applicationContext)
        user = preferences.getUser()
        if (!isUserBlank(user)) {
            setupTextView(user)
            btnPrefSave.text = resources.getString(R.string.sp_change)
        } else {
            btnPrefSave.text = resources.getString(R.string.sp_save)
        }
    }

    private fun setupTextView(user: User) {
        tvPrefName.text = user.name
        tvPrefEmail.text = user.email
        tvPrefAge.text = user.age.toString()
        tvPrefPhone.text =  user.phone
        tvPrefHobby.text = if (user.hasReadingHobby) "Membaca" else "Tidak Membaca"
    }

    private fun isUserBlank(user: User): Boolean {
        return user.name.isNullOrBlank() &&
                user.email.isNullOrBlank() &&
                user.age == 0 &&
                user.phone.isNullOrBlank() &&
                !user.hasReadingHobby
    }
}
