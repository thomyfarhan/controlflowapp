package id.co.iconpln.controlflowapp.sharedPreferences

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_shared_preferences_form.*

class SharedPreferencesFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER = "user"
        const val EXTRA_TYPE_FORM = "extra_type_form"
        const val EXTRA_RESULT = "extra_result"
        const val RESULT_CODE = 101
        const val TYPE_ADD = 1
        const val TYPE_EDIT = 2
    }

    private lateinit var user: User
    private var formType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences_form)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        getIntentExtra()
        setupFormType(formType)
        setClickButton()
    }

    private fun setupFormType(formType: Int) {
        when(formType) {
            TYPE_ADD -> setupForm("Tambah Baru", "Simpan")
            TYPE_EDIT -> {
                setupForm("Ubah", "Update")
                showPreferenceInForm()
            }
            else -> setupForm("", "")
        }
    }

    private fun getIntentExtra() {
        user = intent.getParcelableExtra(EXTRA_USER) ?: User()
        formType = intent.getIntExtra(EXTRA_TYPE_FORM, 0)
    }

    private fun setupForm(actionBarTitle: String, btnTitle: String) {
        supportActionBar?.title = actionBarTitle
        btnPrefFormSave.text = btnTitle
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

    private fun showPreferenceInForm() {
        etPrefFormName.setText(user.name)
        etPrefFormEmail.setText(user.email)
        etPrefFormAge.setText(user.age.toString())
        etPrefFormPhone.setText(user.phone)
        rbPrefFormReading.isChecked = user.hasReadingHobby
        rbPrefFormNotReading.isChecked = !user.hasReadingHobby
    }

    private fun saveUser(user: User) {
        val preferences = UserPreferences(this)
        preferences.setUser(user)

        val resultIntent = Intent().putExtra(EXTRA_RESULT, user)
        setResult(RESULT_CODE, resultIntent)
        finish()
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnPrefFormSave -> {
                val name = etPrefFormName.text.toString().trim()
                val email = etPrefFormEmail.text.toString().trim()
                val age = etPrefFormAge.text.toString().trim()
                val phone = etPrefFormPhone.text.toString()
                val hasReadingHobby = rgPrefFormHobby.checkedRadioButtonId == R.id.rbPrefFormReading

                when {
                    name.isBlank() -> etPrefFormName.error =
                        resources.getString(R.string.sp_field_required)
                    email.isBlank() -> etPrefFormEmail.error =
                        resources.getString(R.string.sp_field_required)
                    !checkEmail(email) -> etPrefFormEmail.error =
                        resources.getString(R.string.sp_field_email_not_valid)
                    phone.isBlank() -> etPrefFormPhone.error =
                        resources.getString(R.string.sp_field_required)
                    age.isBlank() -> etPrefFormAge.error =
                        resources.getString(R.string.sp_field_required)
                    age.length > 3 -> etPrefFormAge.error =
                        resources.getString(R.string.sp_field_age_not_valid)
                    else -> {
                        with(user) {
                            this.name = name
                            this.email = email
                            this.age = age.toInt()
                            this.phone = phone
                            this.hasReadingHobby = hasReadingHobby
                        }
                        saveUser(user)
                    }
                }
            }
        }
    }

    private fun setClickButton() {
        btnPrefFormSave.setOnClickListener(this)
    }

    private fun checkEmail(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
