package id.co.iconpln.controlflowapp.sharedPreferences

import android.content.Context

internal class UserPreferences(context: Context) {

    companion object {
        private const val PREFS_USER = "user_prefs"
        private const val NAME = "name"
        private const val EMAIL = "email"
        private const val AGE = "age"
        private const val PHONE = "phone"
        private const val HAS_READING_HOBBY = "has_reading_hobby"
    }

    private val preferences = context
        .getSharedPreferences(PREFS_USER, Context.MODE_PRIVATE)

    fun setUser(value: User) {
        val editor = preferences.edit()
        with (editor) {
            putString(NAME, value.name)
            putString(EMAIL, value.email)
            putInt(AGE, value.age)
            putString(PHONE, value.phone)
            putBoolean(HAS_READING_HOBBY, value.hasReadingHobby)
            apply()
        }
    }

    fun getUser(): User {
        val model = User()
        with(model) {
            name = preferences.getString(NAME, "")
            email = preferences.getString(EMAIL, "")
            age = preferences.getInt(AGE, 0)
            phone = preferences.getString(PHONE, "")
            hasReadingHobby = preferences.getBoolean(HAS_READING_HOBBY, false)
        }
        return model
    }
}
