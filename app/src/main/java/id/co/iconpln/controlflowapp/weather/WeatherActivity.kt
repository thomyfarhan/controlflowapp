package id.co.iconpln.controlflowapp.weather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_weather.*

class WeatherActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var weatherViewModel: WeatherViewModel
    private lateinit var adapter: WeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)

        initViewModel()
        showListAdapter()
        setClickButton()
        fetchWeatherData()
    }

    private fun initViewModel() {
        weatherViewModel = ViewModelProviders.of(this, ViewModelProvider.NewInstanceFactory())
            .get(WeatherViewModel::class.java)
    }

    private fun showListAdapter() {
        adapter = WeatherAdapter()
        adapter.notifyDataSetChanged()

        rvWeatherList.layoutManager = LinearLayoutManager(this)
        rvWeatherList.adapter = adapter
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            pbWeatherLoading.visibility = View.VISIBLE
        } else {
            pbWeatherLoading.visibility = View.GONE
        }
    }

    private fun setClickButton() {
        btnWeatherSearch.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnWeatherSearch -> {
                val city = etWeatherCity.text.toString()
                if (!city.isBlank()) {
                    weatherViewModel.setWeather(city)
                    showLoading(true)
                }
            }
        }
    }

    private fun fetchWeatherData() {
        // Get value from View Model's Live Data
        weatherViewModel.getWeathers().observe(this, Observer { weathers ->
            if (!weathers.isNullOrEmpty()) {
                adapter.setWeatherData(weathers)
                showLoading(false)
            }
        })
    }


}
