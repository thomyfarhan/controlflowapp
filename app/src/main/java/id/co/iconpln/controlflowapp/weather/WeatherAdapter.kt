package id.co.iconpln.controlflowapp.weather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.item_list_weather.view.*

class WeatherAdapter: RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    private val weathers = ArrayList<Weather>()

    fun setWeatherData(weatherData: ArrayList<Weather>) {
        weathers.clear()
        weathers.addAll(weatherData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_weather, parent, false)
        return WeatherViewHolder(view)
    }

    override fun getItemCount(): Int {
        return weathers.size
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(weathers[position])
    }

    inner class WeatherViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(weather: Weather) {
            itemView.tvWeatherCity.text = weather.name
            itemView.tvWeatherDesc.text = weather.description
            itemView.tvWeatherTemp.text = weather.temperature
        }
    }


}
