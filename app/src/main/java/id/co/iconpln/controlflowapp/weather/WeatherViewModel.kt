package id.co.iconpln.controlflowapp.weather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import id.co.iconpln.controlflowapp.BuildConfig
import org.json.JSONObject
import java.text.DecimalFormat

class WeatherViewModel: ViewModel() {

    companion object {
        private const val API_KEY = BuildConfig.API_KEY
    }

    private val listWeathers = MutableLiveData<ArrayList<Weather>>()

    internal fun setWeather(city: String) {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Weather>()
        val url = "https://api.openweathermap.org/data/2.5/group?id=$city&units=metric&appid=$API_KEY"

        // Request Weather API
        client.get(url, object: AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray) {

                try {
                    // Get Data List JSON Array
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)

                    val list = responseObject.getJSONArray("list")

                    // Convert JSON Object to Readable data
                    // data is JSON Object is read by its KEY. ex: id, name
                    for (i in 0 until list.length()) {
                        val weather = list.getJSONObject(i)
                        val weatherItem = Weather()

                        val temp = weather.getJSONObject("main").getDouble("temp")

                        with(weatherItem) {
                            id = weather.getInt("id")
                            name = weather.getString("name")
                            currentWeather = weather
                                .getJSONArray("weather")
                                .getJSONObject(0).getString("main")
                            description = weather
                                .getJSONArray("weather")
                                .getJSONObject(0).getString("description")
                            temperature = DecimalFormat("##.##").format(temp)
                        }

                        listItems.add(weatherItem)
                    }

                    listWeathers.postValue(listItems)
                } catch(e: Exception) {
                    Log.d("onFailure", e.message.toString())
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable) {

                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun getWeathers(): LiveData<ArrayList<Weather>> {
        return listWeathers
    }
}
